# Airo Project
집안 가전 기기들을 제어하기 위해 구글홈, 애플 홈 등 다양한 플랫폼들이 만들어지고 있다. 이러한 플랫폼을 이용하여 일반 개발자나 중소기업이 손쉽게 AI 로봇을 제작할 수 있는 플랫폼을 만들고자 함이다. 이 플랫폼을 이용하면 AI모듈을 선택해서 만들 수 있고 다양한 Sensor들을 연동시킬 수 있기에 보다 빠르고 쉽게 로봇 개발이 가능하다.

## 부품
Matrix 8*8 : 로봇의 눈 역활로 상황에 따른 로봇의 감정이나 반응을 표현한다.

Camera : 로봇 머리에 부착하여 로봇이 바라보는 영상을 수집하여 분석하는데 사용한다.

IR Sensor : IR신호를 사용하여 상황에 맞게 집안 가전을 제어한다.

Arduino : 머리 좌/우/상/하 움직임과 Matrix 8*8제어 

Respeaker : 4방향 지향성 마이크를 이용하여 원활하게 음성인식할 수 있게 한다.

Raspberry pi  : 로봇의 두뇌역활로 모든 센서를 제어한다.

## 구조 설명
Arduino가 Servo Motor, Led Matrix를 제어하고 Raspberry pi와 USB Serial 통신으로 명령을 주고 받는 다. Raspberry pi는 Camera와 연결되어 영상입력을 받고 Respeaker를 통해 음성을 입력받는다.Respeaker로 받은 음성은 디지털로 변환되어 Google Assitant API를 통해 분석되고 인식된다.

Google Assitant API에 가전 제어 음성명령을 추가하여 TV 음성명령 이벤트를 피드백 받고 명령 종류에 따라 IR 센서를 사용해서 TV를 제어하게 된다.
Raspberry pi에서 웹서버를 구축하고 웹 어플리케이션을 개발하여 관리 웹페이지를 만들었다.웹서버에서 받은 명령을 Arduino에 전달하기 위해 Command UDP서버를 개발하였고 Command UDP 모듈을 통해 Arduino는 명령받고 센서를 조종하게된다.

![Alt text](./explain.png)

## ai_voice_recognizer
언어 : 파이썬

음성인식 모듈

## airo_3d_modeling
3D 툴 : Sketch Up

Airo 로봇 3D 모델링 파일

## airo_mng_server
언어 : php

관리 제어 사이트

## airo_mng_site
Front-end 테스트

## 시연 영상
[눈동자 제어](https://www.youtube.com/watch?v=Xr9DCAc1IFY)

[머리 동작 제어](https://www.youtube.com/watch?v=RDU_wyY9t7c)

[원격 소리 재생](https://www.youtube.com/watch?v=0j3rHEyWaBM)

[웹 CCTV](https://www.youtube.com/watch?v=-ivEsfv06HQ)

[TV 제어](https://www.youtube.com/watch?v=DLq5wfl0d8E)

[구글 - 유머](https://www.youtube.com/watch?v=eFc7LPCuSIE)

[구글 - 뉴스](https://www.youtube.com/watch?v=9TLzJuQH5Qw)

[구글 - 날씨](https://www.youtube.com/watch?v=sP7RhzcjbVw)
