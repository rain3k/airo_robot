//import * as THREE from './three.module.js';

//import { ColladaLoader } from 'jsm/loaders/ColladaLoader.js';

var ColladaLoader=THREE.ColladaLoader;
var container, clock;
var camera, scene, renderer, elf;

init();
animate();

function setMaterial(obj){
	if(obj.children != null){
		var childobj = obj.children;
		for(var i=0;i<childobj.length;i++){
			if(childobj[i].material != null){
				childobj[i].material.transparent=true;
				childobj[i].material.opacity=0.7;
				childobj[i].material.depthTest=false;
				childobj[i].material.color = new THREE.Color("rgb(255, 255, 255)");
			}else{
				setMaterial(childobj[i]);
			}
		}
	}
}

function init() {

	container = document.getElementById( 'container_robot' );

	camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 0.1, 2000 );
	camera.position.set( 0.35, 0, 0.35 );
	camera.lookAt( 0, 0.05, 0 );

	scene = new THREE.Scene();

	clock = new THREE.Clock();

	// loading manager

	var loadingManager = new THREE.LoadingManager( function () {
		scene.add( elf );

	} );

	// collada

	var loader = new ColladaLoader( loadingManager );
	loader.load( './model/robot.dae', function ( collada ) {
		elf = collada.scene;
		elf.position.y = -0.015;
		elf.position.z = 0.15;
		setMaterial(elf);
	} );


	var ambientLight = new THREE.AmbientLight( 0xcccccc, 0.4 );
	scene.add( ambientLight );

	var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.8 );
	directionalLight.position.set( 1, 1, 0 ).normalize();
	scene.add( directionalLight );

	var screenRate = 1;
	var renderRect = container.getBoundingClientRect();
	renderer = new THREE.WebGLRenderer({ alpha: true } );
	renderer.setClearColor( 0x000000, 0 );
	renderer.sortObjects = false;
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth*screenRate, window.innerHeight*screenRate);
	container.appendChild( renderer.domElement );

	window.addEventListener( 'resize', onWindowResize, false );

}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

	requestAnimationFrame( animate );

	render();

}

function render() {

	var delta = clock.getDelta();

	if ( elf !== undefined ) {

		elf.rotation.z += delta * 0.5;

	}

	renderer.render( scene, camera );

}