#!/usr/bin/env python

# Copyright (C) 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from __future__ import print_function

import argparse
import json
import os.path
import pathlib2 as pathlib
import RPi.GPIO as GPIO
import time
import google.oauth2.credentials
import thread
import serial, struct, sys
import os
from googletts import Translator
import Adafruit_DHT as dht
from py_irsend import irsend
from google.assistant.library import Assistant
from google.assistant.library.event import EventType
from google.assistant.library.file_helpers import existing_file
from google.assistant.library.device_helpers import register_device
from pixels import Pixels, pixels
from alexa_led_pattern import AlexaLedPattern
from google_home_led_pattern import GoogleHomeLedPattern

try:
    FileNotFoundError
except NameError:
    FileNotFoundError = IOError


WARNING_NOT_REGISTERED = """
    This device is not registered. This means you will not be able to use
    Device Actions or see your device in Assistant Settings. In order to
    register this device follow instructions at:

    https://developers.google.com/assistant/sdk/guides/library/python/embed/register-device
"""

DEBUG = 0
CMD_MODE = 2
CMD_QUERY_DATA = 4
CMD_DEVICE_ID = 5
CMD_SLEEP = 6
CMD_FIRMWARE = 7
CMD_WORKING_PERIOD = 8
MODE_ACTIVE = 0
MODE_QUERY = 1

#ser = serial.Serial('/dev/ttyACM0', 9600)

#ser = serial.Serial()
#ser.port = "/dev/ttyUSB0"
#ser.baudrate = 9600

#ser.open()
#ser.flushInput()

byte, data = 0, ""

def dump(d, prefix=''):
    print(prefix + ' '.join(x.encode('hex') for x in d))

def construct_command(cmd, data=[]):
    assert len(data) <= 12
    data += [0,]*(12-len(data))
    checksum = (sum(data)+cmd-2)%256
    ret = "\xaa\xb4" + chr(cmd)
    ret += ''.join(chr(x) for x in data)
    ret += "\xff\xff" + chr(checksum) + "\xab"

    if DEBUG:
        dump(ret, '> ')
    return ret

def process_data(d):
    r = struct.unpack('<HHxxBB', d[2:])
    pm25 = r[0]/10.0
    pm10 = r[1]/10.0
    checksum = sum(ord(v) for v in d[2:8])%256
    return [pm25, pm10]
    #print("PM 2.5: {} μg/m^3  PM 10: {} μg/m^3 CRC={}".format(pm25, pm10, "OK" if (checksum==r[2] and r[3]==0xab) else "NOK"))

def process_version(d):
    r = struct.unpack('<BBBHBB', d[3:])
    checksum = sum(ord(v) for v in d[2:8])%256
    print("Y: {}, M: {}, D: {}, ID: {}, CRC={}".format(r[0], r[1], r[2], hex(r[3]), "OK" if (checksum==r[4] and r[5]==0xab) else "NOK"))

def read_response():
    byte = 0
    while byte != "\xaa":
        byte = ser.read(size=1)

    d = ser.read(size=9)

    if DEBUG:
        dump(d, '< ')
    return byte + d

def cmd_set_mode(mode=MODE_QUERY):
    ser.write(construct_command(CMD_MODE, [0x1, mode]))
    read_response()

def cmd_query_data():
    ser.write(construct_command(CMD_QUERY_DATA))
    d = read_response()
    values = []
    if d[1] == "\xc0":
        values = process_data(d)
    return values

def cmd_set_sleep(sleep=1):
    mode = 0 if sleep else 1
    ser.write(construct_command(CMD_SLEEP, [0x1, mode]))
    read_response()

def cmd_set_working_period(period):
    ser.write(construct_command(CMD_WORKING_PERIOD, [0x1, period]))
    read_response()

def cmd_firmware_ver():
    ser.write(construct_command(CMD_FIRMWARE))
    d = read_response()
    process_version(d)

def cmd_set_id(id):
    id_h = (id>>8) % 256
    id_l = id % 256
    ser.write(construct_command(CMD_DEVICE_ID, [0]*10+[id_l, id_h]))
    read_response()


playingRadio=False
#GPIO.setmode(GPIO.BCM)
#GPIO.setup(13, GPIO.OUT)
#p1 = GPIO.PWM(13, 50)

def conversation_start(threadName,delay):
    irsend.send_once('Samsung_BN59-00940A', ['KEY_MUTE']) 
    os.system('python action_listen_end.py')
    #pixels.wakeup()

def conversation_end(threadName,delay):
    irsend.send_once('Samsung_BN59-00940A', ['KEY_MUTE']) 
    os.system('python action_listen_end.py') 
    #pixels.off()

def listen_effect_sound(threadName,delay):
    print("listen start!!!")
    os.system('aplay /home/pi/emergency030.wav')

def process_event(event):
    """Pretty prints events.

    Prints all events that occur with two spaces between each new
    conversation and a single space between turns of a conversation.

    Args:
        event(event.Event): The current event to process.
    """
    global playingRadio 
    if event.type == EventType.ON_CONVERSATION_TURN_STARTED:
        thread.start_new_thread(listen_effect_sound,("Thread-1",1,)) 
        irsend.send_once('Samsung_BN59-00940A', ['KEY_MUTE']) 
        os.system('python action_listen_start.py')
    print(event)

    if (event.type == EventType.ON_CONVERSATION_TURN_FINISHED and
            event.args and not event.args['with_follow_on_turn']):
        print("End")
        os.system('python action_listen_end.py')
        irsend.send_once('Samsung_BN59-00940A', ['KEY_MUTE'])
        if playingRadio:
            print("radio start")
    if event.type == EventType.ON_RESPONDING_STARTED:
        print("Start")

    if event.type == EventType.ON_RESPONDING_FINISHED:
        print("Start Finish")
    if event.type == EventType.ON_CONVERSATION_TURN_TIMEOUT:
       thread.start_new_thread(conversation_end,("Thread-3",3,))

    if event.type == EventType.ON_DEVICE_ACTION:
        for command, params in event.actions:
            print('Do command', command, 'with params', str(params))
            if command == "com.fingermark.commands.TV":
                object = params['object']
                ctrl = params['ctrl']
                if object == u'RADIO' or object == u'클래식' or object == u'음악':                
                     print("Radio control")                     
                     if ctrl == u'ON': 
                         playingRadio=True 
                         #os.system("/home/pi/assistant-sdk-python/google-assistant-sdk/googlesamples/assistant/library/radio_classi_stop.sh") 
                         #os.system("/home/pi/assistant-sdk-python/google-assistant-sdk/googlesamples/assistant/library/radio_classi_start.sh &")
                     if ctrl == u'OFF':
                         playingRadio=False 
                if object == 'DUST': 
                     #cmd_set_sleep(0)
                     #cmd_set_mode(1);
                     #values = cmd_query_data();
                     translator = Translator('AIzaSyD_Gl6Edqta3Np8zmkX5B6AsEwopGUjbuQ')
                     if values is not None:
                         print("PM2.5: ", values[0], ", PM10: ", values[1])
                         tdust=""
                         if values[0] <= 15:
                             tdust="좋음"
                         if values[0] <= 25 and values[0] >= 16:
                             tdust="보통"
                         if values[0] <= 35 and values[0] >= 26:
                             tdust="나쁨"
                         if values[0] >= 36:
                             tdust="매우나쁨"
                         dust=""
                         if values[1] <= 30:
                             dust="좋음"
                         if values[1] <= 50 and values[1] >= 31:
                             dust="보통"
                         if values[1] <= 70 and values[1] >= 51:
                             dust="나쁨"
                         if values[1] >= 71:
                             dust="매우나쁨"
                         result="현재 미세먼지 농도는 "+str(values[1])+","+dust+"이며 초미세먼지 농도는 "+str(values[0])+","+tdust+"입니다."
                         output=translator.speak(result, "ko-KR", "ko-KR-Standard-A", "LINEAR16", 16000)
                         with open("dustfile.wav", "wb") as f:
                             f.write(output)
                             os.system('aplay /home/pi/assistant-sdk-python/google-assistant-sdk/googlesamples/assistant/library/dustfile.wav')
                if object == 'INFO': 
                     humidity,temperature = dht.read_retry(dht.DHT22,13) 
                     translator = Translator('AIzaSyD_Gl6Edqta3Np8zmkX5B6AsEwopGUjbuQ')
                     result="현재 집온도는 "+str(int(temperature))+"도이고 습도는 "+str(int(humidity))+"% 입니다." 
                     if int(humidity) < 40:
                         result=result+"습도가 낮습니다. 가습기를 켜주세요." 
                     print(result) 
                     output = translator.speak(result, "ko-KR", "ko-KR-Standard-A", "LINEAR16", 16000)
                     with open("file.wav", "wb") as f:
                          f.write(output) 
                     os.system('aplay /home/pi/assistant-sdk-python/google-assistant-sdk/googlesamples/assistant/library/file.wav') 
                if object == 'TV':
                     print("TV control")                     
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_POWER'])
                if object == 'MBC':
                     print("MBC control")
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_1']) 
                     time.sleep(1)
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_1'])
                if object == 'SBS':
                     print("SBS control")
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_5'])
                if object == 'KBS1':
                     print("KBS1 control")
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_9'])
                if object == 'KBS2':
                     print("KBSD control")
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_7']) 
                if object == 'JTBC':
                     print("JTBC control")
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_1'])
                     time.sleep(1)
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_7'])
                if object == 'EBS':
                     print("EBS control")
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_3'])
                if object == 'EBS2':
                     print("EBS2 control")
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_3'])
                if object == 'TVN':
                     print("TVN control")
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_1'])
                     time.sleep(1) 
                     irsend.send_once('Samsung_BN59-00940A', ['KEY_6'])

def main():
    pixels.pattern = GoogleHomeLedPattern(show=pixels.show)
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--device-model-id', '--device_model_id', type=str,
                        metavar='DEVICE_MODEL_ID', required=False,
                        help='the device model ID registered with Google')
    parser.add_argument('--project-id', '--project_id', type=str,
                        metavar='PROJECT_ID', required=False,
                        help='the project ID used to register this device')
    parser.add_argument('--device-config', type=str,
                        metavar='DEVICE_CONFIG_FILE',
                        default=os.path.join(
                            os.path.expanduser('~/.config'),
                            'googlesamples-assistant',
                            'device_config_library.json'
                        ),
                        help='path to store and read device configuration')
    parser.add_argument('--credentials', type=existing_file,
                        metavar='OAUTH2_CREDENTIALS_FILE',
                        default=os.path.join(
                            os.path.expanduser('~/.config'),
                            'google-oauthlib-tool',
                            'credentials.json'
                        ),
                        help='path to store and read OAuth2 credentials')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s ' + Assistant.__version_str__())

    args = parser.parse_args()
    with open(args.credentials, 'r') as f:
        credentials = google.oauth2.credentials.Credentials(token=None,
                                                            **json.load(f))

    device_model_id = None
    last_device_id = None
    try:
        with open(args.device_config) as f:
            device_config = json.load(f)
            device_model_id = device_config['model_id']
            last_device_id = device_config.get('last_device_id', None)
    except FileNotFoundError:
        pass

    if not args.device_model_id and not device_model_id:
        raise Exception('Missing --device-model-id option')

    # Re-register if "device_model_id" is given by the user and it differs
    # from what we previously registered with.
    should_register = (
        args.device_model_id and args.device_model_id != device_model_id)

    device_model_id = args.device_model_id or device_model_id

    with Assistant(credentials, device_model_id) as assistant:
        events = assistant.start()

        device_id = assistant.device_id
        print('device_model_id:', device_model_id)
        print('device_id:', device_id + '\n')

        # Re-register if "device_id" is different from the last "device_id":
        if should_register or (device_id != last_device_id):
            if args.project_id:
                register_device(args.project_id, credentials,
                                device_model_id, device_id)
                pathlib.Path(os.path.dirname(args.device_config)).mkdir(
                    exist_ok=True)
                with open(args.device_config, 'w') as f:
                    json.dump({
                        'last_device_id': device_id,
                        'model_id': device_model_id,
                    }, f)
            else:
                print(WARNING_NOT_REGISTERED)

        for event in events:
            process_event(event)


if __name__ == '__main__':
    main()
